import {
  CodeFilled,
  HomeFilled,
  IdcardFilled,
  MailFilled,
  SignalFilled,
} from "@ant-design/icons";
import React from "react";
import { NavLink } from "react-router-dom";

import "./BottomNav.scss";

function BottomNav() {
  return (
    <div className="bottomnav">
      <NavLink to="/">
        <HomeFilled
          style={{
            fontSize: "30px",
            cursor: "pointer",
            animationName: "downToTop",
            animationDuration: "1s",
            animationTimingFunction: "ease-out",
          }}
        />
      </NavLink>

      <NavLink to="/skills">
        <SignalFilled
          style={{
            fontSize: "30px",
            cursor: "pointer",
            animationName: "downToTop",
            animationDuration: "1s",
            animationTimingFunction: "ease-out",
            animationDelay: "0.2s",
            animationFillMode: "backwards",
          }}
        />
      </NavLink>

      <NavLink to="/projects">
        <CodeFilled
          style={{
            fontSize: "30px",
            cursor: "pointer",
            animationName: "downToTop",
            animationDuration: "1s",
            animationTimingFunction: "ease-out",
            animationDelay: "0.3s",
            animationFillMode: "backwards",
          }}
        />
      </NavLink>

      <NavLink to="/profile">
        <IdcardFilled
          style={{
            fontSize: "30px",
            cursor: "pointer",
            animationName: "downToTop",
            animationDuration: "1s",
            animationTimingFunction: "ease-out",
            animationDelay: "0.4s",
            animationFillMode: "backwards",
          }}
        />
      </NavLink>

      <NavLink to="/contact">
        <MailFilled
          style={{
            fontSize: "30px",
            cursor: "pointer",
            animationName: "downToTop",
            animationDuration: "1s",
            animationTimingFunction: "ease-out",
            animationDelay: "0.5s",
            animationFillMode: "backwards",
          }}
        />
      </NavLink>
    </div>
  );
}

export default BottomNav;
