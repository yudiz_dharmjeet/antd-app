import React from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  AppstoreFilled,
  CloseOutlined,
  GithubOutlined,
} from "@ant-design/icons";

import "./Nav.scss";
import allActions from "../../store/allActions";

function Nav() {
  const dispatch = useDispatch();
  const bottomNav = useSelector((state) => state.main?.bottomNav);

  function openBottomNav() {
    dispatch(allActions.MainAction.openBottomNav());
  }

  function closeBottomNav() {
    dispatch(allActions.MainAction.closeBottomNav());
  }

  return (
    <>
      <div className="nav">
        {bottomNav && (
          <CloseOutlined
            style={{
              fontSize: "30px",
              cursor: "pointer",
              animation: "leftToRight 1s ease-out",
            }}
            onClick={closeBottomNav}
          />
        )}

        {!bottomNav && (
          <AppstoreFilled
            style={{
              fontSize: "30px",
              cursor: "pointer",
              animation: "leftToRight 1s ease-out",
            }}
            onClick={openBottomNav}
          />
        )}

        <h3 className="logo">Dharmjeet</h3>

        <a
          href="https://github.com/dharmjeet05"
          rel="noreferrer"
          target="_blank"
        >
          <GithubOutlined
            style={{
              fontSize: "30px",
              cursor: "pointer",
              animation: "rightToLeft 1s ease-out",
            }}
          />
        </a>
      </div>
    </>
  );
}

export default Nav;
