import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import "./App.scss";
import { BottomNav, Nav } from "./components";
import { Contact, Home, Profile, Projects, Skills } from "./pages";

function App() {
  const bottomNav = useSelector((state) => state.main.bottomNav);

  return (
    <div className="app">
      <div className="container">
        <BrowserRouter>
          <Nav />
          <div
            className={
              bottomNav ? "page-container-height" : "page-container-height-full"
            }
          >
            <Routes>
              <Route path="/" exact element={<Home />} />
              <Route path="/skills" element={<Skills />} />
              <Route path="/projects" element={<Projects />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/contact" element={<Contact />} />
            </Routes>
          </div>
          {bottomNav && <BottomNav />}
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
