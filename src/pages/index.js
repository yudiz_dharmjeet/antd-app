import Home from "./Home/Home";
import Skills from "./Skills/Skills";
import Projects from "./Projects/Projects";
import Profile from "./Profile/Profile";
import Contact from "./Contact/Contact";

export { Home, Skills, Projects, Profile, Contact };
