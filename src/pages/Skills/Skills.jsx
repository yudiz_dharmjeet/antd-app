import { Progress } from "antd";
import React from "react";
import { FaReact, FaNodeJs } from "react-icons/fa";
import { SiNextdotjs, SiMongodb } from "react-icons/si";

import "./Skills.scss";

function Skills() {
  return (
    <div className="skills-main">
      <div className="skills-first">
        <h1>Skills</h1>
        <div className="skills-data">
          <div className="skills-item">
            <Progress type="circle" percent={80} strokeColor="#00D8FF" />
            <h3>
              <FaReact /> Reactjs
            </h3>
          </div>
          <div className="skills-item">
            <Progress type="circle" percent={40} strokeColor="#000000" />
            <h3>
              <SiNextdotjs /> Nextjs
            </h3>
          </div>
          <div className="skills-item">
            <Progress type="circle" percent={20} strokeColor="#6EAE55" />
            <h3>
              <FaNodeJs /> Nodejs
            </h3>
          </div>
          <div className="skills-item">
            <Progress type="circle" percent={20} strokeColor="#269643" />
            <h3>
              <SiMongodb />
              MongoDB
            </h3>
          </div>
        </div>
      </div>

      <div className="skills-second">
        <h1>Skills </h1>
        <div className="skills-data">
          <div className="skills-item">
            <h3>
              <FaReact /> Reactjs
            </h3>
            <Progress percent={80} strokeColor="#00D8FF" />
          </div>
          <div className="skills-item">
            <h3>
              <SiNextdotjs /> Nextjs
            </h3>
            <Progress percent={40} strokeColor="#000000" />
          </div>
          <div className="skills-item">
            <h3>
              <FaNodeJs /> Nodejs
            </h3>
            <Progress percent={20} strokeColor="#6EAE55" />
          </div>
          <div className="skills-item">
            <h3>
              <SiMongodb />
              MongoDB
            </h3>
            <Progress percent={20} strokeColor="#269643" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Skills;
