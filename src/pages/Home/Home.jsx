import { Avatar, Image } from "antd";
import React from "react";

import "./Home.scss";
import profilePic from "../../assets/images/Dharmjeet Vaishnav.jpeg";

function Home() {
  return (
    <div className="home">
      <div className="avatar">
        <Avatar
          src={<Image src={profilePic} style={{ width: 200 }} />}
          style={{ width: 200, height: 200 }}
          draggable="true"
        />
      </div>

      <h1>Dharmjeet Vaishnav</h1>
      <h4>Front-End Developer</h4>
    </div>
  );
}

export default Home;
