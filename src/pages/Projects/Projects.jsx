import React from "react";

import "./Projects.scss";

import { Card, Avatar, Carousel, Image } from "antd";
import {
  EllipsisOutlined,
  GithubOutlined,
  LaptopOutlined,
} from "@ant-design/icons";

const { Meta } = Card;
// const popover_content = (
//   <div>
//     <p>Content</p>
//     <p>Content</p>
//   </div>
// );

function Projects() {
  return (
    <div className="projects">
      <div className="projects-heading">
        <h1>Projects </h1>
      </div>
      <div className="project-cards">
        <Card
          style={{
            width: 330,
            boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
          }}
          cover={
            <Carousel autoplay>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://railsware.com/blog/wp-content/uploads/2019/07/Why-we-use-ReactJS-for-our-projects-Illustration.jpg"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://images.indepth.dev/images/2020/12/Under-the-hood-of-React-Hooks.png"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://miro.medium.com/max/1400/1*y7JWssYTe4y6Fgmk6SfoKg.png"
                />
              </div>
            </Carousel>
          }
          actions={[
            <GithubOutlined key="setting" />,
            <LaptopOutlined key="live" />,
            <EllipsisOutlined key="ellipsis" />,
          ]}
          className="project-card"
        >
          <Meta
            avatar={
              <Avatar src="https://www.mindinventory.com/blog/wp-content/uploads/2018/07/reactjs-1200x600.jpg" />
            }
            title="React App"
            description="Demo of React App"
          />
        </Card>
        <Card
          style={{
            width: 330,
            boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
          }}
          cover={
            <Carousel autoplay>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://railsware.com/blog/wp-content/uploads/2018/09/2400%D1%851260-rw-blog-node-js.png"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://www.anchorsoftacademy.com/media/courseimg/node-api.jpg"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://strongloop.com/blog-assets/2014/01/threading_node.png"
                />
              </div>
            </Carousel>
          }
          actions={[
            <GithubOutlined key="setting" />,
            <LaptopOutlined key="live" />,
            <EllipsisOutlined key="ellipsis" />,
          ]}
          className="project-card"
        >
          <Meta
            avatar={
              <Avatar src="https://images.g2crowd.com/uploads/product/image/social_landscape/social_landscape_f0b606abb6d19089febc9faeeba5bc05/nodejs-development-services.png" />
            }
            title="Nodejs App"
            description="Demo of nodejs app"
          />
        </Card>
        <Card
          style={{
            width: 330,
            boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
          }}
          cover={
            <Carousel autoplay>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://blog.codeminer42.com/wp-content/uploads/2021/02/nextjs-cover.jpg"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/UVlzv5dlRqaKaswsfUPk"
                />
              </div>
              <div>
                <Image
                  width="100%"
                  alt="example"
                  src="https://www.freecodecamp.org/news/content/images/2022/01/Screen-Shot-2022-01-14-at-2.17.23-PM.png"
                />
              </div>
            </Carousel>
          }
          actions={[
            <GithubOutlined key="setting" />,
            <LaptopOutlined key="live" />,
            <EllipsisOutlined key="ellipsis" />,
          ]}
          className="project-card"
        >
          <Meta
            avatar={
              <Avatar src="https://camo.githubusercontent.com/92ec9eb7eeab7db4f5919e3205918918c42e6772562afb4112a2909c1aaaa875/68747470733a2f2f6173736574732e76657263656c2e636f6d2f696d6167652f75706c6f61642f76313630373535343338352f7265706f7369746f726965732f6e6578742d6a732f6e6578742d6c6f676f2e706e67" />
            }
            title="Nextjs App"
            description="Demo of nextjs app"
          />
        </Card>
      </div>
    </div>
  );
}

export default Projects;
