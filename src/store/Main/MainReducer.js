import {
  OPEN_BOTTOM_NAV,
  CLOSE_BOTTOM_NAV,
  TOGGLE_BOTTOM_NAV,
} from "../action-types";

const initialState = {
  bottomNav: false,
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_BOTTOM_NAV:
      return {
        ...state,
        bottomNav: true,
      };

    case CLOSE_BOTTOM_NAV:
      return {
        ...state,
        bottomNav: false,
      };

    case TOGGLE_BOTTOM_NAV:
      return {
        ...state,
        bottomNav: !state.bottomNav,
      };

    default:
      return state;
  }
};

export default main;
