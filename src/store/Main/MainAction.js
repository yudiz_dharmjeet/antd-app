import {
  OPEN_BOTTOM_NAV,
  CLOSE_BOTTOM_NAV,
  TOGGLE_BOTTOM_NAV,
} from "../action-types";

const openBottomNav = () => {
  return {
    type: OPEN_BOTTOM_NAV,
  };
};

const closeBottomNav = () => {
  return {
    type: CLOSE_BOTTOM_NAV,
  };
};

const toggleBottomNav = () => {
  return {
    type: TOGGLE_BOTTOM_NAV,
  };
};

export default {
  openBottomNav,
  closeBottomNav,
  toggleBottomNav,
};
